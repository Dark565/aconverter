# ACONVERTER Makefile

src_path=src
linker_settings=$(src_path)/entry.ld

ASM=nasm
ASM_Flags=-felf64
LD=ld
LD_Flags=-m elf_x86_64 -T $(linker_settings)

out=Aconverter

source_o=$(patsubst %.asm,%.o,$(subst /,-,$(wildcard src/*.asm)))

all: build

build: $(source_o)
	ld $(source_o) -o $(out) $(LD_Flags)
	

clean:
	rm -rf $(source_o) $(out)

$(source_o):
	$(ASM) $(patsubst %.o,%.asm,$(subst -,/,$@)) -o $@ $(ASM_Flags)

.phony: build all clean