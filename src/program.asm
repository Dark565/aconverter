[BITS 64]

%include "src/config.inc"

section .text

extern itoa

;(dd - argc, dq - argv)
global convert
convert:

; function stack frame

push rbp
mov rbp, rsp

; if argc = 1, break function

mov eax, [rbp+16]
cmp eax, 0x1
jle .end

; get binary value from first argument

mov rbx, [rbp+20]
mov rcx, [rbx]

; set arguments for 'itoa' function

lea rsp, [rbp-20]
lea rbx, [rbp-10]

mov QWORD [rsp], rcx
mov QWORD [rsp+8], rbx
mov DWORD [rsp+16], 10

; call itoa

call itoa

; set registers for syscall

mov rsi, rax

mov rdx, rbp
sub rdx, rsi

mov rdi, SYS_STDOUT_FD
mov rax, SYS_WRITE

; do syscall

syscall

; end function

.end:

mov rsp, rbp
pop rbp
ret