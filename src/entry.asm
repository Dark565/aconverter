[BITS 64]

%include "src/config.inc"

section .text

extern convert
extern itoa

global start
start:

; call program func
;call convert
; call exit

lea rsi, [rsp-10]

mov dword [rsi-4], 10
mov qword [rsi-12], rsi
mov qword [rsi-20], rsp

lea rsp, [rsi-20]

call itoa

add rsp, 20

mov rsi, rax
lea rdx, [rsp+10]

sub rdx, rax

mov rax, SYS_WRITE
mov rdi, SYS_STDOUT_FD

syscall

mov rax, SYS_EXIT
xor rdi, rdi

syscall


