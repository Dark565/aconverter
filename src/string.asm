[BITS 64]

section .text

; dq index (dq - long64, dq - buffer, dd - buffer_size)

global itoa
itoa:

mov esi, [rsp+24]
mov rax, [rsp+8]
mov rcx, [rsp+16]
mov rbx, 0xA

; if number = 0, jmp to special procedure

cmp rax, 0x0
je .zero

.loop:

; increase array index by 1

dec rsi
cmp rsi, 0x0
jl .suc

; divide by 10 to get remainder which is a cipher

div rbx

; add a cipher (remainder + '0') to the tab

lea rdi, [rcx+rsi]
add dh, '0'
mov BYTE [rdi], dh

; repeat the operation

jmp .loop

.zero:

; add the zero to the tab

cmp rsi, 0x0
je .end

mov BYTE [rcx], '0'
mov rax, rcx

jmp .end

.suc:
    mov rax, rdi

; end the function

.end:

ret